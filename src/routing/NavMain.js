import React from 'react'


import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom'

export default function NavMain() {
    return (
        <div>
            <nav>
                <ul className="ui five item menu">
                    <li className="item">
                        <Link to="/listbooks">list books</Link>
                    </li>
                    <li className="item">
                        <Link to="/listauthors">list authors</Link>
                    </li>
                    <li className="item">
                        <Link to="/book">Book</Link>
                    </li>
                    <li className="item">
                        <Link to="/contact">Kontakt</Link>
                    </li>
                    <li className="item">
                        <Link to="/privacy">Datenschutz</Link>
                    </li>
                </ul>
            </nav>
        </div>
    )
}
