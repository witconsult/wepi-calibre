import React from 'react'
import {BrowserRouter as Router} from 'react-router-dom'

import Header from './layout/Header'
import Body from './layout/Body'
import Footer from './layout/Footer'


import './App.scss'

function App() {
    return (
        <div className="ui container">
            <Router>
                <Header />
                <Body />
                <Footer />
            </Router>
        </div>
    )
}

export default App
