import React from 'react'
import useConnectApi from '../connect/ConnectApi'
import ShowBookDetail from './ShowBookDetail'
import QuickSearchBar from './QuickSearchBar'

export default function ShowBookList(props) {

    const data = useConnectApi('get', 'books')

    if (!data) { return null }
    //TODO Graylog console.log(data.books)

    return(
        <div className="ui segment">
            <QuickSearchBar />
            {data.books.map(book => (
                <ShowBookDetail key={book.id} bookpreview={book} />
            ))}
        </div>
    )
}
