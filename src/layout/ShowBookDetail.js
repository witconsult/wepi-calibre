import React, {useState} from 'react'
import {bookApi} from '../connect/ConnectApi'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'

export default function ShowBookDetail(props) {

    const [viewState, setViewState] = useState('list')
    const [book, setBook] = useState()

    const getBook = function () {
        try{
            return bookApi('get', 'book?id=' + props.bookpreview.id, (data) => {setBook(data)})
        } catch(e) {
            console.log('Error')
        }
    }

    const showListView = (event) => {
        if ( viewState === 'list') {
            getBook()
            setViewState('details')
        } else {
            setViewState('list')
        }
    }

    function renderRating(rating) {
        let stars = []
        for (var i = 0; i < rating; i++) {
            stars.push('⭐')
        }
        return stars
    }

    const thumb = function() {
        return 'http://localhost:4000/thumb?id=' + props.bookpreview.id
    }

    const bookFile = function() {
        return 'http://localhost:4000/bookFile?id=' + props.bookpreview.id
    }

    return(
        <div className='item'>
            { props.bookpreview.thumbnails && props.bookpreview.thumbnails.length && <img alt='2do' src={props.bookpreview.thumbnails[0].url} className="ui tiny image" />}
            <div className="ui raised segment">
                <div onClick={showListView} key={props.bookpreview.id} className="header clickable">
                    <h3>{props.bookpreview.title}</h3>
                </div>
                {( viewState === 'details' && book ? //TODO Loading spinner @&&
                    <div>
                        <p>{book.authors[0].name}</p>
                        <div>
                            <div className="wepi-thumb">
                                <img src={thumb()} className="ui small image"></img>
                            </div>
                        </div>
                        <div>
                            <a className="ui mycolor float right ribbon label">#{props.bookpreview.id}</a>
                            <span className="wepi-side wepi-side-language">{book.language}</span>
                            <span className="wepi-side wepi-side-rating">{renderRating(book.rating)}</span>
                            <a><div className="wepi-side wepi-side-print"><i className="print icon"></i>print</div></a>
                            <a href={bookFile()}><div className="wepi-side wepi-side-download"><i className="download icon"></i> {book.format}</div></a>
                        </div>
                        <div className="wepi-description">{ReactHtmlParser(book.comments.slice(0,250))}...</div>
                    </div>
                    :
                    <span>{props.bookpreview.author_sort}</span>
                )}
            </div>
        </div>
    )
}
