import React from 'react'
import logo from '../logo.svg'
import NavMain from '../routing/NavMain'

export default function Header(props) {
    return(
        <header className="ui center aligned header">
            <img src={logo} className="App-logo" alt="Header logo" />
            <NavMain />
        </header>
    )
}
