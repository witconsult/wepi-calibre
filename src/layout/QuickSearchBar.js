import React, {useState} from 'react'

export default function QuickSearchBar() {

    const [query, setQuery] = useState('')

    const handleSubmit = (e) => {
        console.log('onSubmit')
        e.preventDefault()
    }

    const handleBlur = () => {
        console.log('onBlur')
    }

    return (
        <div className="ui segment">
            <form className="ui form" onSubmit = {handleSubmit}>
                <input placeholder="Buchname" value={query} onChange={(e) => setQuery(e.target.value)} onBlur={handleBlur} />
                <button className="ui button">Suchen</button>
            </form>
        </div>
    )
}
