import React from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

import ShowBookList from '../layout/ShowBookList'
import ShowBook from '../layout/ShowBook'

export default function Body(props) {
    return(
        <div>
            {/* A <Switch> looks through its children <Route>s and
                renders the first one that matches the current URL. */}
            <Switch>
                <Route path="/listbooks">
                    <ShowBookList />
                </Route>
                <Route path="/listauthors">
                    <ShowBookList />
                </Route>
                <Route path="/contact">
                    <Contact />
                </Route>
                <Route path="/privacy">
                    <Privacy />
                </Route>
                <Route path="/book">
                    <ShowBook />
                </Route>
                <Route path="/">
                    <ShowBookList />
                </Route>
            </Switch>
        </div>
    )
}

function Contact() {
    //TODO
    return <div><br />
        <h2>Impressum</h2>
        <p className="ui existing segment"><h3>Ipsum Essum</h3>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
        <p className="ui existing segment"><h3>Ipsum Essum</h3>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
        <p className="ui existing segment"><h3>Ipsum Essum</h3>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
    </div>
}

function Privacy() {
    //TODO
    return <div><br />
        <h2>Datenschutzerklärung</h2>
        <p className="ui existing segment"><h3>Datum Essum</h3>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
        <p className="ui existing segment"><h3>Datum Essum</h3>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
        <p className="ui existing segment"><h3>Datum Essum</h3>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
    </div>
}
