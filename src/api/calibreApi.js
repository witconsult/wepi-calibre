const sqlite3 = require('sqlite3').verbose()
const express = require('express')
var cors = require('cors')
const sqlite = require('aa-sqlite')
const server = express()

const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')

/**
 * API Settings
 * TODO: make configurable
 */
const port = 4000
const libroot = 'P:/www/wepi-calibre/library/'
const dbfile = 'metadata.db'
server.use(cors())

server.get('/', (req, res) => {
    res.send('ⱳepi-calibre API, check <a href="/docs">/docs</a> for documentation')
})

server.get('/book',function(request,response){
    getBook(request, response)
})

server.get('/books',function(request,response){
    getBooks(request, response)
})

server.get('/bookFile',function(request,response){
    sendBookFile(request, response)
})

server.get('/thumb',function(request,response){
    sendThumbnail(request, response)
})

server.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))


/** DEPRECIATED (unused)
 * getDetail - fetches one detail from the database
 *
 * @param  {type} from   database table (authors)
 * @param  {type} detail database detail (name)
 * @param  {type} id     id of the database entry
 * @return {type}        returns the requested detail
 */
async function getDetail(from, detail, id) {
    console.log(await sqlite.open(libroot + dbfile))
    //console.log('SQL: SELECT ' + detail + ' FROM ' + from + ' WHERE id="' + id + '"')
    const r = await sqlite.get('SELECT ' + detail + ' FROM ' + from + ' WHERE id="' + id + '"')
    sqlite.close()
    return r[detail]
}

/**
 * getBooks - fetches a list of books depending on query parameters
 *
 * @param  {type} request
 * @param  {type} response
 * @return {json} returns a list of books, available parameters via query:
 *          q: search query
 *          limit: limit results to author | title
 */
//TODO expand for items per page functionality
async function getBooks(request, response) {
    console.log(await sqlite.open(libroot + dbfile))
    let books
    if (!request.query.q) {
        console.log('/books API: no query string passed')
        books = await sqlite.all('SELECT id, title, author_sort FROM books')
    } else if ( !request.query.limit ) {
        console.log('/books API: query string: ' + request.query.q + ' LIMIT: none ')
        books = await sqlite.all('SELECT id, title, author_sort FROM books WHERE author_sort LIKE "%' + request.query.q + '%" UNION SELECT id, title, author_sort FROM books WHERE title LIKE "%' + request.query.q + '%"')
    } else if ( request.query.limit == 'title') {
        console.log('/books API: query string: ' + request.query.q + ' LIMIT: title')
        books = await sqlite.all('SELECT id, title, author_sort FROM books WHERE title LIKE "%' + request.query.q + '%"')
    } else if ( request.query.limit == 'author') {
        console.log('API: query string: ' + request.query.q + ' LIMIT: author')
        books = await sqlite.all('SELECT id, title, author_sort FROM books WHERE author_sort LIKE "%' + request.query.q + '%"')
    } else {
        console.log('/books API: failed [query: ' + request.query.limit + ' ] [limit: ' + request.query.limit + ']')
    }
    sqlite.close()
    await response.json({
        'books' : books
    })
}

/**
 * sendThumbnail - description
 *
 * @param  {type} request api request
 * @param  {type} responseapi response
 * @return {type} returns the book thumbnail as a jpg file
 */
async function sendThumbnail(request, response) {
    console.log(await sqlite.open(libroot + dbfile))
    let path = await sqlite.get('SELECT path FROM books WHERE id="' + request.query.id + '"')
    let filename = await sqlite.get('SELECT name FROM data WHERE book="' + request.query.id + '"')
    let format = await sqlite.get('SELECT format FROM data WHERE book="' + request.query.id + '"')
    sqlite.close()
    //TODO not unix compatible - dashes and format case are not accounted for!
    let fileName = libroot + path.path + '/cover.jpg'
    //TODO LOG: console.log(fileName)

    await response.sendFile(fileName, function (err) {
        if (err) {
            console.log('error sending file')
        } else {
            console.log('Sent:', fileName)
        }
    })
}

/**
 * sendBookFile - description
 *
 * @param  {type} request api request
 * @param  {type} response api response
 * @return {file} returns the book file as a download response
 */
async function sendBookFile(request, response) {

    console.log(await sqlite.open(libroot + dbfile))
    let path = await sqlite.get('SELECT path FROM books WHERE id="' + request.query.id + '"')
    let filename = await sqlite.get('SELECT name FROM data WHERE book="' + request.query.id + '"')
    let format = await sqlite.get('SELECT format FROM data WHERE book="' + request.query.id + '"')
    sqlite.close()
    //TODO not unix compatible - dashes and format case are not accounted for!
    let fileName = libroot + path.path + '/' + filename.name + '.' + format.format.toLowerCase()
    //TODO: LOG console.log(fileName)

    await response.download(fileName, filename.name + '.' + format.format.toLowerCase(), function (err) {
        if (err) {
            console.log('error sending file')
        } else {
            console.log('Sent:', fileName)
        }
    })
}

/**
 * getBook - description
 *
 * @param  {type} request  description
 * @param  {type} response description
 * @return {type}   returns the book information
 *                  - requires: id via query
 */
async function getBook(request, response) {
    console.log(await sqlite.open(libroot + dbfile))
    let title = await sqlite.get('SELECT title FROM books WHERE id="' + request.query.id + '"')
    let authors = await sqlite.all('SELECT name FROM authors INNER JOIN books_authors_link ON authors.id = books_authors_link.author AND books_authors_link.book=' + request.query.id )
    let publisher = await sqlite.get('SELECT name FROM publishers INNER JOIN books_publishers_link ON publishers.id = books_publishers_link.publisher AND books_publishers_link.book=' + request.query.id )
    let language = await sqlite.get('SELECT languages.lang_code FROM languages INNER JOIN books_languages_link ON languages.id = books_languages_link.lang_code AND books_languages_link.book=' + request.query.id )
    let format = await sqlite.get('SELECT format FROM data WHERE book="' + request.query.id + '"')
    let size = await sqlite.get('SELECT uncompressed_size FROM data WHERE book="' + request.query.id + '"')
    let timestamp = await sqlite.get('SELECT timestamp FROM books WHERE id="' + request.query.id + '"')
    let pubdate = await sqlite.get('SELECT pubdate FROM books WHERE id="' + request.query.id + '"')
    let rating = await sqlite.get('SELECT rating FROM books_ratings_link WHERE book="' + request.query.id + '"')
    let tags = await sqlite.all('SELECT tag FROM books_tags_link WHERE book="' + request.query.id + '"')
    let comments = await sqlite.get('SELECT text FROM comments WHERE id="' + request.query.id + '"')
    sqlite.close()
    await response.json({
        id: request.query.id,
        title: title.title,
        authors: authors,
        publisher: publisher.name,
        language: language.lang_code,
        timestamp: timestamp.timestamp,
        pubdate: pubdate.pubdate,
        tags,
        rating: rating.rating,
        format: format.format,
        comments: comments.text
    })
}

server.listen(port, () => {
    console.log(`Express Server listening at ${port}`)
})
